/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import modelo.Modelo;
import vista.Vista;

/**
 *
 * @author Andres
 */
public class Controlador implements ActionListener {

    private Vista view;
    private Modelo model;

    public Controlador(Vista view, Modelo model) {
        this.view = view;
        this.model = model;
        this.view.btnmultiplicar.addActionListener(this);
    }

    public void iniciar() {
        view.setTitle("Seno con Serie de Taylor");
        view.setLocationRelativeTo(null);
        view.txtresultado.setEnabled(false);
    }

    public void actionPerformed(ActionEvent e) {
        model.setX((int) Double.parseDouble(view.txt1.getText()));
        model.setN((int) Double.parseDouble(view.txt2.getText()));
        model.resformula();
        view.txtresultado.setText(String.valueOf(model.getResultado()));
    }
}
