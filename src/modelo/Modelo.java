/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Andres
 */
public class Modelo {

    private double n;
    private double x;
    private double resultado;

    public double getN() {
        return n;
    }

    public void setN(double n) {
        this.n = n;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getResultado() {
        return resultado;
    }

    public void setResultado(double resultado) {
        this.resultado = resultado;
    }

    private int resigno(int numsigno) {
        int resultado = 0;
        if (numsigno % 2 == 0) {
            resultado = 1;
        } else {
            resultado = -1;
        }
        return resultado;
    }

    private double resfactorial(double numfactorial) {
        double factorial = 1;
        for (double i = numfactorial; i > 0; i--) {
            factorial = factorial * i;
        }
        return factorial;
    }
//Solucion de formula del seno llamando los metodos de resfactorial y resigno
    public double resformula() {
        this.resultado = 0;
        for (int i = 0; i < this.n + 1; i++) {
            int num = 0;
            double radianes = 0, signo = 0, factorial = 0;

            signo = ((2 * i) + 1);
            num = resigno(i);
            factorial = resfactorial(signo);
            radianes = Math.pow(Math.toRadians(this.x), signo);
            this.resultado = ((num / factorial) * radianes) + this.resultado;

        }
        return this.resultado;

    }
}
